import followers.Follower;
import followers.IncomeFollower;
import org.junit.Test;

public class TestService {
    @Test
    public void testClothesShop(){
        ClothesShop clothesShop = new ClothesShop("Nike Air", "Puma T-Shirt");

        Follower follower1 = new IncomeFollower("follower1@gmail.com");
        Follower follower2 = new IncomeFollower("follower2@gmail.com");
        Follower follower3 = new IncomeFollower("follower3@gmail.com");

        clothesShop.follow("Nike Air", follower1);
        clothesShop.follow("Nike Air", follower2);
        clothesShop.follow("Puma T-Shirt", follower3);
        clothesShop.follow("Puma T-Shirt", follower1);

        clothesShop.deliver("Nike Air", 5);

        clothesShop.deliver("Puma T-Shirt", 4);
    }
}
