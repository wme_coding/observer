package followers;

public class IncomeFollower implements Follower{
    String email;

    public IncomeFollower(String email){
        this.email = email;
    }


    @Override
    public void update(String clothesName) {
        sendEmail("Heyo, there is a new income of your favourite " + clothesName + " check this out!");
        System.out.println("Done for " + email + " " +clothesName);
    }

    private void sendEmail(String message){
        //sending email
    }
}
