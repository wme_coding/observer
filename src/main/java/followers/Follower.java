package followers;

public interface Follower{
    void update(String clothesName);
}
