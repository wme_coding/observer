import followers.Follower;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClothesShop {
    private Map<String, List<Follower>> followers;
    private Map<String, Integer> amountOfClothes;

    public ClothesShop(String ... initialClothes){
        followers = new HashMap<>();
        amountOfClothes = new HashMap<>();
        for(String s: initialClothes){
            followers.put(s, new ArrayList<Follower>());
            amountOfClothes.put(s, 0);
        }
    }

    public void follow(String clothe, Follower follower){
        followers.get(clothe).add(follower);
    }

    public void unfollow(String clothe, Follower follower){
        followers.get(clothe).remove(follower);
    }

    public void deliver(String clothe, int amount){
        if(!amountOfClothes.containsKey(clothe)){
            amountOfClothes.put(clothe, amount);
            followers.put(clothe, new ArrayList<Follower>());
            return;
        }

        amountOfClothes.put(clothe, amountOfClothes.get(clothe) + amount);
        List<Follower> toSendEmail = followers.get(clothe);
        for(Follower f: toSendEmail){
            f.update(clothe);
        }
    }
}
